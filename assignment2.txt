Assignment-2


Assignment 1: The below program is to guess the correct number between 1 to 100

import random

def checkTheUserInput(number):
    
    if number.isdigit() and 1<= int(number) <=100:
        return True
    else:
        return False
        
        
def compareUserInputWithRandom(userInput,randomNumber):
    
    if userInput<randomNumber:
        userInput=input("Too low. Guess again")
        
    elif userInput>randomNumber:
        userInput=input("Too High. Guess again")
       
    return userInput


def main():
    
    randomNumber=random.randint(1,100)
    condition=False
    userInput=input("Guess a number between 1 and 100:")
    chancesTakenToGussTheNumber=0
    
    while not condition:
        
        if not checkTheUserInput(userInput):
            userInput=input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            chancesTakenToGussTheNumber+=1
            userInput=int(userInput)
            
        userInput = compareUserInputWithRandom(userInput,randomNumber)
        
        if (userInput==randomNumber):
            print("You guessed it in",chancesTakenToGussTheNumber,"guesses!")
            condition=True


main()
